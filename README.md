### Installation

```sh
$ npm install ts-error-logger
```

### API

| Method | Arguments | Description                             | interface                                                       |
|--------|-----------|-----------------------------------------|-----------------------------------------------------------------|
| config | options   | Set url to request. Default = '/logger' | baseUrl?:string; client?:string; server?:string; logic?:string; |
| client | ...args   | JSON like arguments to server           | JSON like                                                       |
| server | ...args   | JSON like arguments to server           | JSON like                                                       |
| logic  | ...args   | JSON like arguments to server           | JSON like                                                       |

### Version
0.0.11

License
----

ISC
