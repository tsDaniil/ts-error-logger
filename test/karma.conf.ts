declare var module;

module.exports = (config) => {
    config.set({
        browsers: ['Chrome'],

        frameworks: ['mocha', 'commonjs'],

        files: [
            {
                pattern: '../**/**.map',
                included: false
            },
            {
                pattern: '../**/**.ts',
                included: false
            },
            './test.js',
            '../src/*.js',
            '../node_modules/platform/platform.js',
            '../node_modules/expect.js/index.js',
            '../node_modules/ts-request-test/index.js',
            '../node_modules/ts-request-test/src/Request.js',
        ],

        preprocessors: {
            '../src/*.js': ['commonjs'],
            '../test/*.js': ['commonjs'],
            '../node_modules/ts-request-test/index.js': ['commonjs'],
            '../node_modules/ts-request-test/src/Request.js': ['commonjs']
        },

        client: {
            mocha: {
                reporter: 'html', // change Karma's debug.html to the mocha web reporter
                ui: 'bdd'
            }
        }
    });
};