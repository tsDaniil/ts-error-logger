/// <reference path="./../typings/tsd.d.ts" />
/// <reference path="./interface.d.ts" />

import Logger = require('../src/Logger');
import XMLHttpRequest = require('ts-request-test');

window['XMLHttpRequest'] = XMLHttpRequest;

describe('send', () => {

    Logger.config({
        'baseUrl': '/'
    });

    XMLHttpRequest.config({
        '/?client': {}
    });

    it('client', (done) => {

        XMLHttpRequest.toDebug((xhr) => {
            if (xhr.readyState == 4) {
                expect(xhr['url']).to.be('/?client');
                expect(xhr.status).to.be(200);
                done();
            }
        });
        Logger.client(new Error("тест"), 'тест', {test: 'тест'}, ['тест']);

    });

});