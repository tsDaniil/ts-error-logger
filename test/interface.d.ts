declare module 'ts-request-test' {

    interface Instance extends XMLHttpRequest {

    }

    interface Request extends XMLHttpRequest {
        toDebug(callback:(xhr:Instance) => void);
        config(options:Options)
    }

    interface Options {
        [url:string]: {
            types?:Array<string>;
            response?: string;
            time?: number;
        }
    }

    var foo:Request;
    export = foo;
}