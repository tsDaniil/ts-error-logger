/// <reference path="typings/tsd.d.ts" />
/// <reference path="./interface/FsPromise.d.ts" />

import gulp = require('gulp');
import concat = require('gulp-concat');
import fs = require('ts-fs-promise');
import promiseModule = require("es6-promise");
var uglify = require('gulp-uglify');
var Promise = promiseModule.Promise;

gulp.task('default', () => {

    return gulp.src('./src/Logger.js')
        .pipe(concat('Logger.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build'));

});

gulp.task('upVersion', (done) => {

    var upVersion = (text:string) => {
        return text.split('.').map((version, index) => {
            return index === 2 ? (+version) + 1 : version;
        }).join(".");
    };

    Promise.all([
        new Promise((resolve) => {
            fs.readJSON('./package.json').then((data) => {
                data.version = upVersion(data.version);
                fs.writeFile('./package.json', JSON.stringify(data, <any>'', 2)).then(resolve);
            });
        }),
        new Promise((resolve) => {
            fs.readFile('./README.md', 'utf8').then((readme:string) => {
                fs.writeFile('./README.md', readme.replace(/\d{1,3}\.\d{1,3}\.\d{1,3}/, (str) => {
                    return upVersion(str);
                })).then(resolve);
            });
        })
    ]).then(() => {
        done();
    });

});

gulp.task('prePublish', ['default', 'upVersion']);