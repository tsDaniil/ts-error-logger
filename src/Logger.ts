/// <reference path="../typings/tsd.d.ts" />

class Logger {

    private clientData:ClientData;
    private options:Options;

    constructor() {
        this.initialize();
    }

    public config(options):void {
        options = options || {};
        Logger.each(options, (url:string, key:string) => {
            if (url) {
                this.options[key] = url;
            }
        });
    }

    public client(...args):void {
        this.send(Logger.getMessage(arguments), 'client');
    }

    public server(...args):void {
        this.send(Logger.getMessage(arguments), 'server');
    }

    public logic(...args):void {
        this.send(Logger.getMessage(arguments), 'logic');
    }

    private send(message:string, type:string):void {
        var error = this.createError(message, type);
        console.group('Error logger');
        console.error(error);
        console.groupEnd();
        this.request(JSON.stringify(error), type);
    }

    private createError(message:string, type:string) {
        return {
            type: type,
            message: message,
            clientInfo: this.clientData,
            time: Date.now()
        }
    }

    private request(error:string, type:string) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', this.getUrl(type), true);
        xhr.send(error);
    }

    private getUrl(type):string {
        return (this.options[type] || this.options.baseUrl) + "?" + type;
    }

    private initialize():void {
        this.initializeClientData();
        this.initializeOptions();
    }

    private initializeOptions():void {
        this.options = {
            baseUrl: '/logger'
        };
    }

    private initializeClientData():void {
        this.clientData = {
            os: platform.os,
            layout: platform.layout,
            version: platform.version,
            name: platform.name,
            product: platform.product,
            manufacturer: platform.manufacturer,
            description: platform.description
        };
    }

    private static getMessage(args):string {
        return Array.prototype.map.call(args, (arg) => {

            if (Logger.isError(arg)) {
                return Logger.parseError(arg);
            }

            if (typeof arg == "string" || typeof arg == "number") {
                return arg;
            } else {
                try {
                    return JSON.stringify(arg);
                } catch (e) {
                    console.warn('Не удалось распарсить аргумент!');
                    return arg.toString();
                }
            }
        }).join('\n');
    }

    private static parseError(error:Error) {
        return [error.name, error.message, error['stack']].join('\n');
    }

    private static isError(some):boolean {
        return some instanceof Error;
    }

    private static each(obj:Object, callback:(url:string, key:string) => void):void {
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                callback(obj[key], key);
            }
        }
    }

}

var logger = new Logger();
export = logger;

interface ClientData {
    os: PlatformOS;
    layout: string;
    version: string;
    name: string;
    product: string;
    manufacturer: string;
    description: string;
}

interface Options {
    baseUrl?: string;
    client?: string;
    server?: string;
    logic?: string;
}